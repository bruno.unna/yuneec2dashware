package com.unna.yuneec;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Converts CSV telemetry information from a Yuneec controller (ST10+) to a format understandable by DashWare.
 * <p>
 * Created by bruno on 04/03/2017.
 */
public class Converter {
    private static Logger logger = Logger.getLogger(Converter.class.getName());
    private static Double first = null;
    private static final SimpleDateFormat parser = new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS");

    public static void main(String[] args) {
        if (args.length != 1) {
            logger.info("Required argument <input_file_name> is missing.");
        }
        new Converter().run(args[0]);
    }

    private void run(String fileName) {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileName))) {
            final String header = reader.readLine();
            if (header.startsWith(",")) {
                System.out.println("timestamp" + header);
            }
            reader.lines().forEach((String row) -> {
                try {
                    processRow(row);
                } catch (ParseException e) {
                    logger.severe(e.getMessage());
                }
            });
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }

    private static void processRow(String s) throws ParseException {
        String[] data = s.split(",");
        if (first == null) {
            first = toSeconds(data[0]);
        }

        System.out.print(toSeconds(data[0]) - first);
        for (int i = 1; i < data.length; i++) {
            System.out.print("," + data[i]);
        }
        System.out.println();
    }

    private static Double toSeconds(String datum) throws ParseException {
        Date date = parser.parse(datum);
        return date.getTime() / 1000.0;
    }
}
